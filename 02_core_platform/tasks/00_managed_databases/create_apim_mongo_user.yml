- name: Create a temporary Container with a MongoDB Client
  kubernetes.core.k8s:
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    namespace: '{{ inv_am.apim.ns_name }}'
    state: present
    definition:
      apiVersion: v1
      kind: Pod
      metadata:
        name: temp-mongodb-client
      spec:
        containers:
          - name: web
            image: percona/percona-server-mongodb:4.4

- name: Wait till the MongoDB Pod is created
  kubernetes.core.k8s_info:
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    namespace: '{{ inv_am.apim.ns_name }}'
    kind: Pod
    wait: yes
    name: temp-mongodb-client
    wait_sleep: 10
    wait_timeout: 360

- name: Get Gravitee MongoDB Admin User credentials
  no_log: '{{ ansible_debug.no_log }}'
  kubernetes.core.k8s_info:
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    namespace: '{{ inv_am.apim.ns_name }}'
    kind: Secret
    name: '{{ inv_mngd_db.gravitee_mongo.admin_k8s_secret }}'
  register: gravitee_mongo_admin_user

- name: Store MongoDB Admin User Credentials
  no_log: '{{ ansible_debug.no_log }}'
  set_fact:
    mongodb_gravitee_admin_user: "{{ gravitee_mongo_admin_user.resources[0].data.MONGODB_USER_ADMIN_USER | b64decode }}"
    mongodb_gravitee_admin_password: "{{ gravitee_mongo_admin_user.resources[0].data.MONGODB_USER_ADMIN_PASSWORD | b64decode }}"

- name: Generate the random username for mongo client
  no_log: '{{ ansible_debug.no_log }}'
  set_fact:
    apim_mongo_username: "{{ lookup('password', '/dev/null length=8 chars=ascii_letters') }}"

- name: Generate the random password for mongo client
  no_log: '{{ ansible_debug.no_log }}'
  set_fact:
    apim_mongo_userpassword: "{{ lookup('password', '/dev/null length=15 chars=ascii_letters') }}"

- name: Connect to MongoDB with Admin credentials and create a client user
  no_log: '{{ ansible_debug.no_log }}'
  kubernetes.core.k8s_exec:
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    namespace: '{{ inv_am.apim.ns_name }}'
    pod: temp-mongodb-client
    command: mongo "mongodb+srv://{{ mongodb_gravitee_admin_user }}:{{ mongodb_gravitee_admin_password }}@{{ inv_mngd_db.gravitee_mongo.db_address }}/admin?replicaSet=rs0&ssl=false" --eval "db.getSiblingDB('admin').createUser({{ lookup('template', '00_managed_databases/create_gravitee_mongodb_user.json.j2') }} )"
  register: command_status
  ignore_errors: True

- name: If an error by creating the client user
  no_log: '{{ ansible_debug.no_log }}'
  debug:
    var: command_status
  when: command_status.return_code != 0

- name: Deploy Gravitee MongoDB User Secret
  no_log: '{{ ansible_debug.no_log }}'
  kubernetes.core.k8s:
    definition: "{{ item }}"
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    namespace: '{{ inv_am.apim.ns_name }}'
    state: present
  loop:
    - "{{ lookup('template', 'templates/10_apim_stack/deployment/gravitee-mongo-user-secret.yml') | from_yaml }}"
  when: command_status.return_code == 0

- name: Delete the temporary Container with a MongoDB Client
  kubernetes.core.k8s:
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    namespace: '{{ inv_am.apim.ns_name }}'
    state: absent
    definition:
      apiVersion: v1
      kind: Pod
      metadata:
        name: temp-mongodb-client
      spec:
        containers:
          - name: web
            image: percona/percona-server-mongodb:4.4