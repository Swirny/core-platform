---
- name: Set default facts for K8S
  ansible.builtin.set_fact:
    k8s_config: "{{ K8S_KUBECONFIG_PATH }}/{{ inv_ml.ns_kubeconfig }}"
    k8s_context: "{{ inv_k8s.config.context }}"
    k8s_namespace: "{{ inv_ml.ns_name }}"

- name: "[Helm] Create the Monitoring Grafana Installation"
  include_tasks: ansible-templates/k8s_tasks/k8s-helm.yml
  vars:
    helm_repo_name: "{{ ml_grafana.helm_repo_name }}"
    helm_repo_url: "{{ ml_grafana.helm_repo_url }}"
    helm_chart_name: "{{ ml_grafana.helm_chart_name }}"
    helm_release_name: "{{ ml_grafana.helm_release_name }}"
    helm_chart_reference: "{{ helm_repo_name }}/{{ helm_chart_name }}"
    helm_chart_version: "{{ ml_grafana.helm_chart_version }}"
    helm_values_file: "09_monitoring_logging/grafana-values.yaml.j2"
    helm_create_namespace: "{{ inv_ml.ns_create }}"

# - name: Add grafana Helm repo
#   kubernetes.core.helm_repository:
#     name: "{{ ml_grafana.helm_repo_name }}"
#     url: "{{ ml_grafana.helm_repo_url }}"

# - name: Install Helm package with values loaded from template
#   kubernetes.core.helm:
#     name: "{{ ml_grafana.helm_release_name }}"
#     chart_ref: "{{ ml_grafana.helm_repo_name }}/{{ ml_grafana.helm_chart_name }}"
#     kubeconfig: "{{ k8s_config }}"
#     context: "{{ k8s_context }}"
#     release_namespace: "{{ k8s_namespace }}"
#     update_repo_cache: true
#     create_namespace: "{{ inv_ml.ns_create }}"
#     values: "{{ lookup('template', '09_monitoring_logging/grafana-values.yaml.j2') | from_yaml }}"

# - name: Let Grafana a chance to start
#   pause:
#     seconds: 120

- name: "Wait for Grafana to come up"
  uri:
    url: "https://monitoring.{{ DOMAIN }}"
    status_code: 200
  register: result
  until: result.status == 200
  retries: 80
  delay: 10

- name: Ensure temp directory exists
  file:
    path: /tmp/udp/monitoring/prometheus_dashboard_files/
    recurse: yes
    state: directory
    mode: '0755'

- name: Copy Dashboard Folder
  copy:
    src: 09_monitoring_logging/prometheus_dashboard_files/
    dest: /tmp/udp/monitoring/prometheus_dashboard_files/
    mode: '0666'

- name: Create a folder
  community.grafana.grafana_folder:
    url: "https://monitoring.{{ DOMAIN }}"
    url_username: "{{ inv_ml.grafana.admin }}"
    url_password: "{{ inv_ml.grafana.password }}"
    title: "Prometheus"
    state: present

- name: (Re-)Import Grafana Dashboards
  community.grafana.grafana_dashboard:
    grafana_url: "https://monitoring.{{ DOMAIN }}"
    url_username: "{{ inv_ml.grafana.admin }}"
    url_password: "{{ inv_ml.grafana.password }}"
    folder: "Prometheus"
    state: present
    commit_message: Updated by ansible
    overwrite: yes
    path: "{{ item }}"
  register: ml_grafana_import
  retries: 5
  delay: 5
  until: ml_grafana_import is succeeded
  with_fileglob:
    - "/tmp/udp/monitoring/prometheus_dashboard_files/*.json"

- name: Create loki datasource
  no_log: '{{ ansible_debug.no_log }}'
  community.grafana.grafana_datasource:
    name: "Loki"
    grafana_url: "https://monitoring.{{ DOMAIN }}"
    grafana_user: "{{ inv_ml.grafana.admin }}"
    grafana_password: "{{ inv_ml.grafana.password }}"
    org_id: "1"
    ds_type: "loki"
    access: proxy
    ds_url: "http://monitoring-loki:3100"
