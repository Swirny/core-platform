---
- name: Set default facts for K8S
  ansible.builtin.set_fact:
    k8s_config: "{{ K8S_KUBECONFIG_PATH }}/{{ inv_dm.ns_kubeconfig }}"
    k8s_context: "{{ inv_k8s.config.context }}"
    k8s_namespace: "{{ inv_dm.ns_name }}"

- name: Create a k8s namespace
  kubernetes.core.k8s:
    name: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    api_version: v1
    kind: Namespace
    state: present
  when: inv_dm.ns_create == "true"

## Secret to be used as MinIO Root Credentials
- name: Deploy MinIO Root Credentials
  kubernetes.core.k8s:
    state: present
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: minio-creds-secret
        namespace: "{{ k8s_namespace }}"
      type: Opaque
      data:
        accesskey: "{{ inv_dm.minio.tenant.accesskey | b64encode }}"
        secretkey: "{{ inv_dm.minio.tenant.secretkey | b64encode }}"

## Secret to be used for MinIO Console
- name: Deploy minio Console Secret
  kubernetes.core.k8s:
    state: present
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: console-secret
        namespace: "{{ k8s_namespace }}"
      type: Opaque
      data:
        CONSOLE_PBKDF_PASSPHRASE: "{{ inv_dm.minio.tenant.console_pbkdf_passphrase | b64encode }}"
        CONSOLE_PBKDF_SALT: "{{ inv_dm.minio.tenant.console_pbkdf_salt | b64encode }}"
        CONSOLE_ACCESS_KEY: "{{ inv_dm.minio.tenant.console_access_key | b64encode }}"
        CONSOLE_SECRET_KEY: "{{ inv_dm.minio.tenant.console_secret_key | b64encode }}"

## MinIO Tenant Definition
- name: Deploy MinIO Tentant Definition
  kubernetes.core.k8s:
    state: present
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition:
      apiVersion: minio.min.io/v2
      kind: Tenant
      metadata:
        name: minio
        namespace: "{{ k8s_namespace }}"
        ## Optionally pass labels to be applied to the statefulset pods
        labels:
          app: minio
        ## Annotations for MinIO Tenant Pods
        annotations:
          prometheus.io/path: /minio/prometheus/metrics
          prometheus.io/port: "9000"
          prometheus.io/scrape: "true"

        ## If a scheduler is specified here, Tenant pods will be dispatched by specified scheduler.
        ## If not specified, the Tenant pods will be dispatched by default scheduler.
        # scheduler:
        #  name: my-custom-scheduler

      spec:
        ## Registry location and Tag to download MinIO Server image
        image: "{{ dm_minio_tenant.image_repository }}:{{ dm_minio_tenant.image_tag }}"
        imagePullPolicy: "{{ dm_minio_tenant.image_pullPolicy }}"

        ## Secret with credentials to be used by MinIO Tenant.
        ## Refers to the secret object created above.
        credsSecret:
          name: minio-creds-secret

        ## Specification for MinIO Pool(s) in this Tenant.
        pools:
            ## Servers specifies the number of MinIO Tenant Pods / Servers in this pool.
            ## For standalone mode, supply 1. For distributed mode, supply 4 or more.
            ## Note that the operator does not support upgrading from standalone to distributed mode.
          - servers: 4

            ## volumesPerServer specifies the number of volumes attached per MinIO Tenant Pod / Server.
            volumesPerServer: 1

            ## This VolumeClaimTemplate is used across all the volumes provisioned for MinIO Tenant in this
            ## Pool.
            volumeClaimTemplate:
              metadata:
                name: data
              spec:
                storageClassName: minio-storage
                accessModes:
                  - ReadWriteOnce
                resources:
                  requests:
                    storage: 10Gi

            ## Used to specify a toleration for a pod
            # tolerations:
            #  - effect: NoSchedule
            #    key: dedicated
            #    operator: Equal
            #    value: storage

            ## nodeSelector parameters for MinIO Pods. It specifies a map of key-value pairs. For the pod to be
            ## eligible to run on a node, the node must have each of the
            ## indicated key-value pairs as labels.
            ## Read more here: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/
            # nodeSelector:
            #   disktype: ssd

            ## Affinity settings for MinIO pods. Read more about affinity
            ## here: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity.
            # affinity:
            #   nodeAffinity:
            #     requiredDuringSchedulingIgnoredDuringExecution:
            #       nodeSelectorTerms:
            #       - matchExpressions:
            #         - key: kubernetes.io/hostname
            #           operator: In
            #           values:
            #           - hostname1
            #           - hostname2

            # Configure resource requests and limits for MinIO containers
            resources:
              requests:
                cpu: 250m
                memory: 512Mi
              limits:
                cpu: 500m
                memory: 1Gi

            ## Configure security context 
            securityContext:
              fsGroup: 1000
              runAsUser: 1000
              runAsGroup: 1000
              runAsNonRoot: true

        ## Mount path where PV will be mounted inside container(s).
        mountPath: /export
        ## Sub path inside Mount path where MinIO stores data.
        # subPath: /data

        ## Use this field to provide a list of Secrets with external certificates. This can be used to to configure
        ## TLS for MinIO Tenant pods. Create secrets as explained here:
        ## https://github.com/minio/minio/tree/master/docs/tls/kubernetes#2-create-kubernetes-secret
        # externalCertSecret:
        #   - name: tls-ssl-minio
        #     type: kubernetes.io/tls

        ## Enable automatic Kubernetes based certificate generation and signing as explained in
        ## https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster
        requestAutoCert: false

        ## Enable S3 specific features such as Bucket DNS which would allow `buckets` to be
        ## accessible as DNS entries of form `<bucketname>.minio.default.svc.cluster.local`
        s3:
          ## This feature is turned off by default
          bucketDNS: false

        ## This field is used only when "requestAutoCert" is set to true. Use this field to set CommonName
        ## for the auto-generated certificate. Internal DNS name for the pod will be used if CommonName is
        ## not provided. DNS name format is *.minio.default.svc.cluster.local
        certConfig:
          commonName: ""
          organizationName: []
          dnsNames: []

        ## PodManagement policy for MinIO Tenant Pods. Can be "OrderedReady" or "Parallel"
        ## Refer https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/#pod-management-policy
        ## for details.
        podManagementPolicy: Parallel

        ## serviceMetadata allows passing additional labels and annotations to MinIO and Console specific 
        ## services created by the operator.
        serviceMetadata:
          minioServiceLabels:
            label: minio-svc
          minioServiceAnnotations:
            v2.min.io: minio-svc
          consoleServiceLabels:
            label: console-svc
          consoleServiceAnnotations:
            v2.min.io: console-svc

        ## Add environment variables to be set in MinIO container (https://github.com/minio/minio/tree/master/docs/config)
        env:
        - name: MINIO_IDENTITY_OPENID_CONFIG_URL
          value: "{{ IDM_ENDP_ISSUER }}/.well-known/openid-configuration"
        - name: MINIO_IDENTITY_OPENID_CLIENT_ID
          value: "{{ IDM_CLIENT.MINIO }}"
        - name: MINIO_IDENTITY_OPENID_CLIENT_SECRET
          value: "{{ IDM_CLIENT_SECRET_MINIO }}"
        - name: MINIO_IDENTITY_OPENID_SCOPES
          value: "openid,profile,email"

        ## PriorityClassName indicates the Pod priority and hence importance of a Pod relative to other Pods.
        ## This is applied to MinIO pods only.
        ## Refer Kubernetes documentation for details https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/#priorityclass/
        # priorityClassName: high-priority

        ## Define configuration for Console (Graphical user interface for MinIO)
        ## Refer https://github.com/minio/console
        console:
          image: "{{ dm_minio_tenant.image_console_repository }}:{{ dm_minio_tenant.image_console_tag }}"
          replicas: 1
          consoleSecret:
            name: console-secret
          securityContext:
            fsGroup: 1000
            runAsUser: 1000
            runAsGroup: 2000
            runAsNonRoot: true
          env:
            - name: CONSOLE_IDP_URL
              value: "{{ IDM_ENDP_ISSUER }}"
            - name: CONSOLE_IDP_CLIENT_ID
              value: "{{ IDM_CLIENT.MINIO }}"
            - name: CONSOLE_IDP_SECRET
              value: "{{ IDM_CLIENT_SECRET_MINIO }}"
            - name: CONSOLE_IDP_CALLBACK
              value: "https://minio-tenant-console.{{ DOMAIN }}.{{ DOMAIN }}/oauth_callback"

- name: Deploy minio test client console ingress
  kubernetes.core.k8s:
    state: present
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition:
      apiVersion: networking.k8s.io/v1
      kind: Ingress
      metadata:
        name: minio-t1-ingress-console
        namespace: "{{ k8s_namespace }}"
        annotations:
          kubernetes.io/ingress.class: "{{ inv_k8s.ingress_class }}"
          kubernetes.io/tls-acme: "true"
          nginx.ingress.kubernetes.io/proxy-buffer-size: 10k
      spec:
        tls:
        - hosts:
          - "minio-tenant.{{ DOMAIN }}"
          secretName: "minio-tenant.{{ DOMAIN }}"
        rules:
        - host: "minio-tenant.{{ DOMAIN }}"
          http:
            paths:
            - path: /
              pathType: Prefix
              backend:
                service:
                  name: minio-console
                  port:
                    number: 9090

- name: Deploy minio test client ingress
  kubernetes.core.k8s:
    state: present
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition:
      apiVersion: networking.k8s.io/v1
      kind: Ingress
      metadata:
        name: minio-t1-ingress
        namespace: "{{ k8s_namespace }}"
        annotations:
          cert-manager.io/cluster-issuer: "{{ inv_k8s.cert_manager.prod_issuer_name }}"
          kubernetes.io/ingress.class: "{{ inv_k8s.ingress_class }}"
          kubernetes.io/tls-acme: "true"
          nginx.ingress.kubernetes.io/proxy-buffer-size: 10k
      spec:
        tls:
        - hosts:
          - "minio-tenant-console.{{ DOMAIN }}"
          secretName: "minio-tenant-console.{{ DOMAIN }}"
        rules:
        - host: "minio-tenant-console.{{ DOMAIN }}"
          http:
            paths:
            - path: /
              pathType: Prefix
              backend:
                service:
                  name: minio
                  port:
                    number: 80